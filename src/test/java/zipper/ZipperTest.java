package zipper;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.google.common.truth.Truth;

@Ignore
public class ZipperTest {
	Zipper zipper;
	String file = "/Users/volker.benders/Documents/jax.pdf";
	
	@Before
	public void init(){
		zipper = new Zipper();
	}
	
	@Test
	public void stripFileNameFromExtension() throws Exception {
	 
		String name = zipper.stripExtension(file);
		Truth.assertThat(name).isEqualTo("jax");
	}
	
	@Test
	public void getFilePath() throws Exception {
		String path = zipper.getPathFromFile(file);
		Truth.assertThat(path).isEqualTo("/Users/volker.benders/Documents");
		
	}
	
	@Test
	public void buildExportFileName() throws Exception {
		String expectedZipFileName = "/Users/volker.benders/Documents/jax.zip";
		String fileName = zipper.buildZipFileName(file);
		Truth.assertThat(fileName).isEqualTo(expectedZipFileName);
	}
	
	@Test
	public void createZip() throws Exception {
		
		String inputFile = "/Users/volker.benders/Documents/Cvs2Git.pptx";
		
		zipper.zipper(inputFile);	
	}
}
