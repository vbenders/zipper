package zipper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Zipper {

	public void zipper(String fileName) throws IOException{
		System.out.println("Writing '" + fileName + "' to zip file");

		ZipOutputStream zos = getZipFileOutputStream(fileName);
		FileInputStream fis = getFileInputStream(fileName);
		ZipEntry zipEntry = new ZipEntry(fileName);
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
		zos.close();
	}

	private FileInputStream getFileInputStream(String fileName) throws FileNotFoundException {
		File file = new File(fileName);
		FileInputStream fis = new FileInputStream(file);
		return fis;
	}

	private ZipOutputStream getZipFileOutputStream(String fileName) throws FileNotFoundException {
		FileOutputStream fos = new FileOutputStream(buildZipFileName(fileName));
		ZipOutputStream zos = new ZipOutputStream(fos);
		return zos;
	}
	
	public String stripExtension(String fileName){
		return fileName.substring(fileName.lastIndexOf('/')+1, fileName.lastIndexOf('.'));
	}

	public String getPathFromFile(String file) {
		return file.substring(0, file.lastIndexOf('/'));
	}

	public String buildZipFileName(String file) {
		return getPathFromFile(file)+"/"+stripExtension(file)+".zip";
	}
	
}
