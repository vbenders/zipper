package zipper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipFiles {
	private static final String SEPARATOR = System.getProperty("file.separator"); 
	private static String zipFile = "/Users/volker.benders/tmp/atest.zip";

	public static ZipOutputStream getZipOutputStream(String zipFile) throws FileNotFoundException{
		return getZipOutputStream(getFileOutputStream(zipFile));
	}
	public static ZipOutputStream getZipOutputStream(FileOutputStream fos) throws FileNotFoundException{
		ZipOutputStream zos = new ZipOutputStream(fos);
		return zos;
	}
	
	public static FileOutputStream getFileOutputStream(String file) throws FileNotFoundException{
		return new FileOutputStream(file);
	}
	
	public List<File> getListOfFilesToZip(String dirName){
		File dir = new File(dirName);
		File[] listFiles = dir.listFiles();
		List<File>files = Arrays.asList(listFiles);
		return files;
	}
	public List<String> getListOfFilesToZip(){
		String file1 = "/Users/volker.benders/Documents/Cvs2Git.pptx";
		String file2 = "/Users/volker.benders/Dropbox/sync/Ebboks/Puppet 3 Cookbook [eBook].pdf";
		
		List<String>files = new ArrayList<String>();
		files.add(file1);
		files.add(file2);
		return files;
	}
	/**
	 * @param args
	 * @throws Exception
	 */
	public static void main(String[] args) throws Exception {
		ZipFiles zipFiles = new ZipFiles();
		
		List<String> listOfFilesToZip = zipFiles.getListOfFilesToZip();
		zipFiles.compress(listOfFilesToZip);
		
//		try {
//			FileOutputStream fos = getFileOutputStream(zipFile);
//			ZipOutputStream zos = getZipOutputStream(fos);
//			String file1 = "/Users/volker.benders/Documents/Cvs2Git.pptx";
//			String file2 = "/Users/volker.benders/Dropbox/sync/Ebboks/Puppet 3 Cookbook [eBook].pdf";
//			
//			List<String>files = new ArrayList<String>();
//			
//			addNewFileToZipFile(file1, zos);
//			addNewFileToZipFile(file2, zos);
//			zos.close();
//			fos.close();
//			System.err.println("fertich");
//		} catch (FileNotFoundException e) {
//			System.err.println("FNF: " + e);
//			e.printStackTrace();
//		} catch (IOException e) {
//			System.err.println("ioex: " + e);
//			e.printStackTrace();
//		}

	}
	
	private void compress(List<String> listOfFilesToZip) throws IOException {
		ZipOutputStream zipOutputStream = getZipOutputStream("/Users/volker.benders/tmp/listOfZippedFiles.zip");
		for (String fileToCompress : listOfFilesToZip) {
			addNewFileToZipFile(fileToCompress, zipOutputStream);
		}
		
	}
	public String buildZipFileName(String dirName){
		
		dirName = stripTrailingFileSeparator(dirName);
		return dirName+".zip";
	}
	public String stripTrailingFileSeparator(String dirName) {
		if (dirName.endsWith(SEPARATOR)){
			dirName = dirName.substring(0, dirName.indexOf(SEPARATOR));
		}
		return dirName;
	}
	public static void addDirToZipFile(String dirName) throws Exception{
		File f = new File(dirName);
		if (!f.isDirectory()){
			throw new Exception(dirName + " is no folder");
		}
		ZipOutputStream zos = getZipOutputStream("/tmp/ZipThis.zip");
		
		for (File file : f.listFiles()){
			addNewFileToZipFile(file.getAbsolutePath(), zos);
			System.err.println("Add '"+file+"' to zip...");
		}
	}

	public static void addNewFileToZipFile(String fileName, ZipOutputStream zos) throws FileNotFoundException, IOException {

		System.out.println("Writing '" + fileName + "' to zip file");

		FileInputStream fis = getFileInputStream(fileName);
		String outputFileName = getOutputFileName(fileName);
		
		ZipEntry zipEntry = new ZipEntry(outputFileName);
		zos.putNextEntry(zipEntry);

		byte[] bytes = new byte[1024];
		int length;
		while ((length = fis.read(bytes)) >= 0) {
			zos.write(bytes, 0, length);
		}

		zos.closeEntry();
		fis.close();
	}

	private static String getOutputFileName(String fileName) {
		return fileName.substring(fileName.lastIndexOf('/')+1);
	}

	private static FileInputStream getFileInputStream(String fileName) throws FileNotFoundException {
		File file = new File(fileName);
		FileInputStream fis = new FileInputStream(file);
		return fis;
	}
}
