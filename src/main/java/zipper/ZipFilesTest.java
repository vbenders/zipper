package zipper;

import static com.google.common.truth.Truth.assertThat;
import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ZipFilesTest {
	private final static String SEPARATOR = System.getProperty("file.separator");
	ZipFiles zipFiles;
	String TEST_RESOURCES = "src/test/resources";
	String EXPECTED_ZIP_FILE_NAME = "src/test/resources.zip";
	
	@Before
	public void init(){
		zipFiles = new ZipFiles();
	}
	
	@Test
	public void getListOfFileToZip_expectFilesFromReourceFolder() throws Exception {
		List<File> listOfFilesToZip = zipFiles.getListOfFilesToZip(TEST_RESOURCES);
		assertThat(listOfFilesToZip).isNotEmpty();
		assertThat(listOfFilesToZip).hasSize(3);
	}
	
	@Test
	public void testGetZipFileName() throws Exception {
		String zipFileName = zipFiles.buildZipFileName(TEST_RESOURCES);
		assertThat(zipFileName).isEqualTo(EXPECTED_ZIP_FILE_NAME);
	}
	
	@Test
	public void stripTrailingFileSeparator_inputPathHasNoTrailingSeparator() throws Exception {
		String stripTrailingFileSeparator = zipFiles.stripTrailingFileSeparator(TEST_RESOURCES);
		assertThat(stripTrailingFileSeparator).doesNotMatch("\\^");
		
	}
	@Test
	public void stripTrailingFileSeparator_inputPathHasTrailingSeparator() throws Exception {
		String stripTrailingFileSeparator = zipFiles.stripTrailingFileSeparator(TEST_RESOURCES+SEPARATOR);
		assertThat(stripTrailingFileSeparator).doesNotMatch("\\^");
		
	}
	
	
}
